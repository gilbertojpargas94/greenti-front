import { Opinion } from './opinion';

export class Restaurant {

	constructor() {
		this.id = -1;
		this.name = '';
		this.opinions = this.opinions_attributes = [];
	}

	id: number;
	name: string;
	opinions: Array < Opinion >;
	opinions_attributes: Array < Opinion >;
}
