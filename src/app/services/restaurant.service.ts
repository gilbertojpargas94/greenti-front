import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Restaurant } from '../models/restaurant';

@Injectable()
export class RestaurantService {

	constructor(private http: HttpClient) { }
	
	getRestaurants(): Observable < Array < Restaurant > > {
		const header = new HttpHeaders().append('Accept', 'application/json');
		return this.http.get < Array < Restaurant > > (environment.apiUrl, { headers: header });
	}

	saveRestaurants(data) {
		return this.http.post(environment.apiUrl, {'restaurants': data });
	}
}
